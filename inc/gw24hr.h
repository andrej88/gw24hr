#ifndef __gw24hr_H__
#define __gw24hr_H__

#include <watch_app.h>
#include <watch_app_efl.h>
#include <Elementary.h>
#include <dlog.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "gw24hr"

#if !defined(PACKAGE)
#define PACKAGE "io.gitlab.andrej88.gw24hr"
#endif

#endif /* __gw24hr_H__ */
