#include <tizen.h>
#include "gw24hr.h"

#define PI 3.1415926535897932384626

typedef struct appdata {
	Evas_Object *win;
	Evas_Object *conform;
	Evas_Object *label;
	Evas_Object* hand_h;
	Evas_Object* hand_m;
	Evas_Object* hand_s;
} appdata_s;

#define TEXT_BUF_SIZE 256

static void _add_hand(Evas_Object** eo, Evas_Object* parent, const char* image_path);

/**
 * Calculates the angle by which to rotate an hour hand on a 24-hour clock,
 * in degrees, with 0 corresponding to "up", going clockwise.
 */
static double _hour24_to_angle(int hour24, int minute, int second);
static double _minute_to_angle(int minute, int second);
static double _second_to_angle(int second);

/**
 * Rotate an image. Taken from the "ClassicWatch" sample.
 *
 * @param[in] image The image you want to rotate
 * @param[in] degree The degree you want to rotate
 * @param[in] cx The rotation's center horizontal position
 * @param[in] cy The rotation's center vertical position
 */
static void _rotate_image(Evas_Object* image, double degree, Evas_Coord cx, Evas_Coord cy);

static void
update_watch(appdata_s *ad, watch_time_h watch_time, int ambient)
{
	char watch_text[TEXT_BUF_SIZE];
	int hour24, minute, second;

	if (watch_time == NULL)
		return;

	watch_time_get_hour24(watch_time, &hour24);
	watch_time_get_minute(watch_time, &minute);
	watch_time_get_second(watch_time, &second);
	if (!ambient) {
		snprintf(watch_text, TEXT_BUF_SIZE, "<align=center>Hello Watch<br/>%02d:%02d:%02d</align>",
			hour24, minute, second);

		_rotate_image(ad->hand_h, _hour24_to_angle(hour24, minute, second), 180, 180);
		_rotate_image(ad->hand_m, _minute_to_angle(minute, second), 180, 180);
		_rotate_image(ad->hand_s, _second_to_angle(second), 180, 180);
	} else {
		snprintf(watch_text, TEXT_BUF_SIZE, "<align=center>Hello Watch<br/>%02d:%02d</align>",
			hour24, minute);
	}

	elm_object_text_set(ad->label, watch_text);
}

static void _rotate_image(Evas_Object *image, double degree, Evas_Coord cx, Evas_Coord cy)
{
	Evas_Map *m = NULL;

	if (image == NULL) {
		dlog_print(DLOG_ERROR, LOG_TAG, "image is NULL");
		return;
	}

	m = evas_map_new(4);
	evas_map_util_points_populate_from_object(m, image);
	evas_map_util_rotate(m, degree, cx, cy);
	evas_object_map_set(image, m);
	evas_object_map_enable_set(image, EINA_TRUE);
	evas_map_free(m);
}

static double _hour24_to_angle(int hour24, int minute, int second)
{
	double outOf1 = (hour24 / 24.0) + (minute / 60.0 / 24.0) + (second / 60.0 / 60.0 / 24.0);
	return outOf1 * 360.0;
}

static double _minute_to_angle(int minute, int second)
{
	double outOf1 = (minute / 60.0) + (second / 60.0 / 60.0);
	return outOf1 * 360.0;
}

static double _second_to_angle(int second)
{
	double outOf1 = second / 60.0;
	return outOf1 * 360.0;
}

static void
create_base_gui(appdata_s *ad, int width, int height)
{
	int ret;
	watch_time_h watch_time = NULL;

	/* Window */
	elm_config_accel_preference_set("opengl");
	ret = watch_app_get_elm_win(&ad->win);
	if (ret != APP_ERROR_NONE) {
		dlog_print(DLOG_ERROR, LOG_TAG, "failed to get window. err = %d", ret);
		return;
	}

	evas_object_resize(ad->win, width, height);

	/* Conformant */
	ad->conform = elm_conformant_add(ad->win);
	evas_object_size_hint_weight_set(ad->conform, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_win_resize_object_add(ad->win, ad->conform);
	evas_object_show(ad->conform);

	/* Label*/
	/*ad->label = elm_label_add(ad->conform);
	evas_object_resize(ad->label, width, height / 3);
	evas_object_move(ad->label, 0, height / 3);
	evas_object_show(ad->label);*/

	/* Hands */
	_add_hand(&(ad->hand_h), ad->conform, "hour.png");
	_add_hand(&(ad->hand_m), ad->conform, "minute.png");
	_add_hand(&(ad->hand_s), ad->conform, "second.png");

	ret = watch_time_get_current_time(&watch_time);
	if (ret != APP_ERROR_NONE)
		dlog_print(DLOG_ERROR, LOG_TAG, "failed to get current time. err = %d", ret);

	update_watch(ad, watch_time, 0);
	watch_time_delete(watch_time);

	/* Show window after base gui is set up */
	evas_object_show(ad->win);
}

static void _add_hand(Evas_Object** eo, Evas_Object* parent, const char* image_path)
{
	char path[TEXT_BUF_SIZE];
	*eo = elm_image_add(parent);
	snprintf(path, TEXT_BUF_SIZE, "%s/%s", app_get_resource_path(), image_path);
	if(!elm_image_file_set(*eo, path, NULL)) {
		dlog_print(DLOG_ERROR, LOG_TAG, "error: could not load image %s", path);
	}
	evas_object_resize(*eo, 360, 360);
	evas_object_show(*eo);
}

static bool
app_create(int width, int height, void *data)
{
	/* Hook to take necessary actions before main event loop starts
		Initialize UI resources and application's data
		If this function returns true, the main loop of application starts
		If this function returns false, the application is terminated */
	appdata_s *ad = data;

	create_base_gui(ad, width, height);

	return true;
}

static void
app_control(app_control_h app_control, void *data)
{
	/* Handle the launch request. */
}

static void
app_pause(void *data)
{
	/* Take necessary actions when application becomes invisible. */
}

static void
app_resume(void *data)
{
	/* Take necessary actions when application becomes visible. */
}

static void
app_terminate(void *data)
{
	/* Release all resources. */
}

static void
app_time_tick(watch_time_h watch_time, void *data)
{
	/* Called at each second while your app is visible. Update watch UI. */
	appdata_s *ad = data;
	update_watch(ad, watch_time, 0);
}

static void
app_ambient_tick(watch_time_h watch_time, void *data)
{
	/* Called at each minute while the device is in ambient mode. Update watch UI. */
	appdata_s *ad = data;
	update_watch(ad, watch_time, 1);
}

static void
app_ambient_changed(bool ambient_mode, void *data)
{
	/* Update your watch UI to conform to the ambient mode */
}

static void
watch_app_lang_changed(app_event_info_h event_info, void *user_data)
{
	/*APP_EVENT_LANGUAGE_CHANGED*/
	char *locale = NULL;
	app_event_get_language(event_info, &locale);
	elm_language_set(locale);
	free(locale);
	return;
}

static void
watch_app_region_changed(app_event_info_h event_info, void *user_data)
{
	/*APP_EVENT_REGION_FORMAT_CHANGED*/
}

int
main(int argc, char *argv[])
{
	appdata_s ad = {0,};
	int ret = 0;

	watch_app_lifecycle_callback_s event_callback = {0,};
	app_event_handler_h handlers[5] = {NULL, };

	event_callback.create = app_create;
	event_callback.terminate = app_terminate;
	event_callback.pause = app_pause;
	event_callback.resume = app_resume;
	event_callback.app_control = app_control;
	event_callback.time_tick = app_time_tick;
	event_callback.ambient_tick = app_ambient_tick;
	event_callback.ambient_changed = app_ambient_changed;

	watch_app_add_event_handler(&handlers[APP_EVENT_LANGUAGE_CHANGED],
		APP_EVENT_LANGUAGE_CHANGED, watch_app_lang_changed, &ad);
	watch_app_add_event_handler(&handlers[APP_EVENT_REGION_FORMAT_CHANGED],
		APP_EVENT_REGION_FORMAT_CHANGED, watch_app_region_changed, &ad);

	ret = watch_app_main(argc, argv, &event_callback, &ad);
	if (ret != APP_ERROR_NONE) {
		dlog_print(DLOG_ERROR, LOG_TAG, "watch_app_main() is failed. err = %d", ret);
	}

	return ret;
}

